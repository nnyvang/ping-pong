﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace IT_Guiden_PONG_Game
{
    public class BonusObjects : Sprite
    {
        private readonly Vector2 outOfBounds = new Vector2(-100.0f, -100.0f);
        private double timeInterval;
        public int flag { get; set; }
        private Texture2D texture;
        private Vector2 location;
        private Rectangle screenBounds;

        public BonusObjects(Texture2D texture, Vector2 location, Rectangle screenBounds)
            : base(texture, location, screenBounds)
        {
            timeInterval = 0;  
        }

        public void setBonusObjects(Texture2D texture, Vector2 location, Rectangle screenBounds)
        {
            this.texture = texture;
            this.location = location;
            this.screenBounds = screenBounds;
        }
/*
        public BonusObjects getBonusObjects()
        {
            return BonusObjects(texture, location, screenBounds);
        }
*/
        private int createdRandomTime(int min, int max)
        {
            var random = new Random();
            return random.Next(min, max);
        }

        public Vector2 generateRandomXYpos()
        {
            var random = new Random();
            var randomXpos = (random.Next(100, 750));
            var randomYpos = (random.Next(100, 400)); //TODO - Create a random position generator and a random time generator
            return new Vector2(randomXpos, randomYpos); 
        }

        public override void Update(GameTime gameTime, GameObjects gameObjects) {
            timeInterval = createdRandomTime(10, 20);
            flag = 0;
            if (gameObjects.Ball.BoundingBox.Intersects(gameObjects.BonusObjects.BoundingBox)) 
            {
                Velocity = new Vector2(-Velocity.X, Velocity.Y);
                //gameObjects.Ball.CheckBounds(vector);
                gameObjects.Ball.Update(gameTime, gameObjects);
                gameObjects.BonusObjects.Update(gameTime, gameObjects);
                gameObjects.Score.PlayerScore++;
                flag = 1;     
            }    
        }
        protected override void CheckBounds() {} 
    }
}
