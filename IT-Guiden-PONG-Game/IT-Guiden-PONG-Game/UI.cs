﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IT_Guiden_PONG_Game
{
    public partial class UI : Form
    {
        private static GameEngine game;

        public UI()
        {
            InitializeComponent();
        }

        [STAThread]
        static void Main()
        {
            Application.Run(new UI());

        }

        private void button1_Click(object sender, EventArgs e)
        {
            game = new GameEngine();
            game.Run();
        }

        
    }

}
