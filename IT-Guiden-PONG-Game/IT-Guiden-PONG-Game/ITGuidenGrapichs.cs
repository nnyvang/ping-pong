﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace IT_Guiden_PONG_Game
{
    public class ITGuidenGrapichs : Sprite
    {
        public ITGuidenGrapichs(Texture2D texture, Vector2 location, Rectangle screenBounds)
            : base(texture, location, screenBounds)
        {
        }

        public override void Update(GameTime gameTime, GameObjects gameObjects) {}

        protected override void CheckBounds() {} 
    }
}
