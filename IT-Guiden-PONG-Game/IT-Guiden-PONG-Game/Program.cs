﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace IT_Guiden_PONG_Game
{
    /**
    /// <summary>
    /// Main class for launching the IT-Guiden.dk Pong Game
    /// </summary>
    public static class Program
    {
        private static GameEngine game;

        /// <summary>
        /// Main thread, starting the game
        /// </summary>
        [STAThread]
        static void Main()
        {
            game = new GameEngine();
            game.Run();
        }
        
    }*/
}
