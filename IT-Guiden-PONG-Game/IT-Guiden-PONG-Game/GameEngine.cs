﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion

namespace IT_Guiden_PONG_Game
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class GameEngine : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D bonusObject = null;
        public static GameEngine instance { get; set; } 

        private ITGuidenGrapichs itgGraphics;
        private BonusObjects bonusObjects;

        private Rectangle gameBoundaries;

        // Primary game objects
        private GameObjects gameObjects;
        private Paddle playerPaddle; 
        private Paddle computerPaddle;
        private Ball ball;
        private Score score;

        public GameEngine()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            instance = this;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Loading Game boundaries used for different calculations
            gameBoundaries = new Rectangle(0, 0, Window.ClientBounds.Width, Window.ClientBounds.Height);


            // Background image - IT-Guiden.dk
            itgGraphics = new ITGuidenGrapichs(Content.Load<Texture2D>("itg-texture"), Vector2.Zero, gameBoundaries);
            
            bonusObject = Content.Load<Texture2D>("plus-5");
            bonusObjects = new BonusObjects(bonusObject, getRandomPos(), gameBoundaries);


            // Image is loaded as a 2D texture from the Content directory.
            playerPaddle = new Paddle(Content.Load<Texture2D>("Paddle-2d-Img"), Vector2.Zero, gameBoundaries, PlayerTypes.Human);
            
            // Loading ComputerPaddle
            var computerPaddleLocation = new Vector2(Window.ClientBounds.Width - Content.Load<Texture2D>("Paddle-2d-Img-Cpu").Width, 0);
            computerPaddle = new Paddle(Content.Load<Texture2D>("Paddle-2d-Img-Cpu"), new Vector2(Window.ClientBounds.Width - Content.Load<Texture2D>("Paddle-2d-Img-Cpu").Width, 0), gameBoundaries, PlayerTypes.Computer);

            // Loading Ball 
            ball = new Ball(Content.Load<Texture2D>("Ball-2d-Img"), Vector2.Zero, gameBoundaries);
            ball.AttachTo(playerPaddle);

            // Loading Score - including font for keeping track of gamescore
            score = new Score(Content.Load<SpriteFont>("GameFont"), gameBoundaries);

            // Load all major GameObjects into one big pile of easy reachable objects
            gameObjects = new GameObjects { PlayerPaddle = playerPaddle, ComputerPaddle = computerPaddle, Ball = ball, Score = score, BonusObjects = bonusObjects };
        }

        private Vector2 getRandomPos()
        {
            // Creating a random position
            var random = new Random();
            var randomXpos = (random.Next(100, 700));
            var randomYpos = (random.Next(100, 400)); //TODO - Create a random position generator and a random time generator
            return new Vector2(randomXpos, randomYpos);
        }
         
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            playerPaddle.Update(gameTime, gameObjects);
            computerPaddle.Update(gameTime, gameObjects);
            ball.Update(gameTime, gameObjects);
            bonusObjects.Update(gameTime, gameObjects);
            score.Update(gameTime, gameObjects);
            if (bonusObjects.flag == 1) {
                if (bonusObject != null)
                {
                    //bonusObject.Dispose();
                    bonusObjects.Update(gameTime, gameObjects);
                }
                bonusObjects = new BonusObjects(bonusObject, getRandomPos(), gameBoundaries);
                bonusObjects.Update(gameTime, gameObjects);
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Transparent);

            // Time for inserting / drawrimg the paddle whvch we created before. Remember placement and color
            spriteBatch.Begin();

            itgGraphics.Draw(spriteBatch);
            playerPaddle.Draw(spriteBatch);
            computerPaddle.Draw(spriteBatch);
            ball.Draw(spriteBatch);
            bonusObjects.Draw(spriteBatch);
            score.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void DisposeBonus(GameObjects gameObjects)
        {
            gameObjects.BonusObjects.Height = 0;
            gameObjects.BonusObjects.Width = 0;
            bonusObject.Dispose();
            //gameObjects.BonusObjects.Location = new Vector2(-200.0f, -200.0f);
        }
    }
}
