﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace IT_Guiden_PONG_Game
{
    /**
     * Sprite class that makes sure to draw itself.
     * 
     * Takes both a texture(image) and a location to place it
    **/

    public abstract class Sprite
    {
        protected readonly Texture2D texture;
        protected readonly Rectangle gameBoundaries;
        public Vector2 Location;
        public Vector2 Velocity { get; protected set; }
        public int Width
        {
            get { return texture.Width; }
            set { }
        }
        public int Height
        {
            get { return texture.Height; }
            set { }
        }
        public Rectangle BoundingBox
        {
            get { return new Rectangle((int)Location.X, (int)Location.Y, Width, Height); }
        }

        /**
         * Constructor for the Sprite
         **/
        public Sprite(Texture2D texture, Vector2 location, Rectangle gameBoundaries)
        {
            this.texture = texture;
            Location = location;
            Velocity = Vector2.Zero;
            this.gameBoundaries = gameBoundaries;
        }

        /**
         * Mases it possible for the SpriteBatch to draw itself
         * */
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, Location, Color.White);
        }



        /**
         * Adds a new velocity to the paddle and checks that it havent moved out of bounds
         */ 
        public virtual void Update(GameTime gameTime, GameObjects gameObjects)
        {
            Location += Velocity;
            CheckBounds();
        }

        protected abstract void CheckBounds();
    }
}
