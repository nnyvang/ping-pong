﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace IT_Guiden_PONG_Game
{
    /**
     * GameObjects holds and handles the main objects of the game
     * which is used for the game play itself.
     **/ 
    public class GameObjects
    {
        // Initializing fields
        public Paddle ComputerPaddle { get; set; }
        public Paddle PlayerPaddle { get; set; }
        public Ball Ball { get; set; }
        public Score Score { get; set; }
        public BonusObjects BonusObjects { get; set; }
    }
}
