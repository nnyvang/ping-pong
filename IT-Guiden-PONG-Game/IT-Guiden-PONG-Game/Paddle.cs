﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace IT_Guiden_PONG_Game
{
    public enum PlayerTypes { 
        Human, 
        Computer
    }

    /**
     * PAddle class that inherits from Sprite (below)
     **/
    public class Paddle : Sprite
    {
        private readonly PlayerTypes playerType;

        public Paddle(Texture2D texture, Vector2 location, Rectangle screenBounds, PlayerTypes playerType) : base(texture, location, screenBounds) 
        {
            this.playerType = playerType;
        }

            /**
             *  MOVEMENT AND INTELLIGENCE OF COMPUTER PLAYER
             *  
             *  The paddle moves with the ball.
             *  If the ball is above the paddle, it moves up.
             *  If the ball is below the paddle, it moves down.
             * 
             *  The <code>reactionThreshold</code> makes the computer think that the
             *  paddle is longer than it actually is, which means it will move too slow
             *  from time to time, making the player able to actualy win the game.
             *  
             *  Human controls:
             *  Specifies the key used for movement:
             *  (up-> right arrow)
             *  (down -> left arrow)
             *  Specifies the velocity (speed) for the movement paddle
             */ 
        public override void Update(GameTime gameTime, GameObjects gameObjects)
        {
            
            // MOVEMENT AND INTELLIGENCE OF COMPUTER PLAYER
            var random = new Random();
            var reactionThreshold = random.Next(30, 90);
            if (playerType == PlayerTypes.Computer) {

                if (gameObjects.Ball.Location.Y + gameObjects.Ball.Height < Location.Y + reactionThreshold) {
                    Velocity = new Vector2(0, -3.5f); // Decrease for faster Computer movement "up"
                    
                }
                if (gameObjects.Ball.Location.Y > Location.Y + Height + reactionThreshold) {
                    Velocity = new Vector2(0, 3.5f); // Increase for faster Computer movement "down"
                }
            }

            // MOVEMENT AND CONTROLS OF HUMAN PLAYER
            if (playerType == PlayerTypes.Human)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Left))
                {
                    Velocity = new Vector2(0, -3.5f); // Decrease for faster Player movement "up"
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Right))
                {
                    Velocity = new Vector2(0, 3.5f); // Increase for faster Player movement "down"
                }
            }
            base.Update(gameTime, gameObjects);
        }

        protected override void CheckBounds()
        {
            Location.Y = MathHelper.Clamp(Location.Y, 0, gameBoundaries.Height - texture.Height);
        }

    }
}
