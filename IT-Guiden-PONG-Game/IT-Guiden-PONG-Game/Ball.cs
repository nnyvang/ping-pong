﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace IT_Guiden_PONG_Game
{
    public class Ball : Sprite
    {
        // Variables
        private Paddle attachedToPaddle;
        private float ballRadius;
        private GameEngine ge;

        /**
         *  
         * 
         **/
        public Ball(Texture2D texture, Vector2 location, Rectangle gameBoundaries) : base(texture, location, gameBoundaries)
        {
            ballRadius = (texture.Width / 2);
            ge = GameEngine.instance;
        }

        /**
         * Used for making sure the ball doesnt goes out the "side" when hitting them.
         * The upper boundary is just 0
         * The lower boundary is less than the gameBoundaries + ball height
         * 
         * If the ball hits the boundaries, the velocity.Y is inverted to give the angle effect
         **/ 
        protected override void CheckBounds()
        {
            if (Location.Y >= (gameBoundaries.Height - texture.Height) || Location.Y <= 0) {
                var newVelocity = new Vector2(Velocity.X, -Velocity.Y);
                Velocity = newVelocity;
            }
        }
        /*
         * Updates the game positions
         * 
         *
         **/
        public override void Update(GameTime gameTime, GameObjects gameObjects)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && attachedToPaddle != null)
            {
                var newVelocity = new Vector2(6.5f, attachedToPaddle.Velocity.Y);
                Velocity = newVelocity;
                attachedToPaddle = null;
            }
            if (attachedToPaddle != null)
            {
                Location.X = attachedToPaddle.Location.X + attachedToPaddle.Width;
                Location.Y = attachedToPaddle.Location.Y;
            }
            else
            {
                if (BoundingBox.Intersects(gameObjects.PlayerPaddle.BoundingBox) || BoundingBox.Intersects(gameObjects.ComputerPaddle.BoundingBox))
                {
                    Velocity = new Vector2(-Velocity.X, Velocity.Y);
                }
                if (gameObjects.Ball.BoundingBox.Intersects(gameObjects.BonusObjects.BoundingBox))
                {
                    /*
                     Fjern den gamle box efter den er blevet ramt. 
                     */
                    ge.DisposeBonus(gameObjects);
                } 
            } 

            base.Update(gameTime, gameObjects);
        }

        public void AttachTo(Paddle paddle)
        {
            attachedToPaddle = paddle;
        }
    }
}
