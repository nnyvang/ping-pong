﻿namespace IT_Guiden_PONG_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    
    public class Score
    {
        private readonly SpriteFont font;
        private readonly Rectangle gameBoundaries;

        /**
         * Public constructor for Score class
         * 
         * @param name="font"
         * @param name="gameBoundraies"
         */
        public Score(SpriteFont font, Rectangle gameBoundraies)
        {
            this.font = font;
            this.gameBoundaries = gameBoundraies;
        }

        public int ComputerScore { get; set; }

        public int PlayerScore { get; set; }

        /**
         * Checking for score.
         * If <code>gameObjects.Ball</code> goes beyond the boundaries
         * to the right, the computer scores and visa versa.
         * 
         * After any score, the score is updated and the ball is attached to
         * the scoring players paddle. 
         */
        public void Update(GameTime gameTime, GameObjects gameObjects) 
        {
            //// Computer score checker
            if (gameObjects.Ball.Location.X + gameObjects.Ball.Width < 0)
            {
                this.ComputerScore++;
                gameObjects.Ball.AttachTo(gameObjects.PlayerPaddle);
            }
            //// Player score checker
            if (gameObjects.Ball.Location.X > this.gameBoundaries.Width)
            {
                this.PlayerScore++;
                gameObjects.Ball.AttachTo(gameObjects.PlayerPaddle);
            }
        }

        /**
         * Draws & updates the score when human or computer scores
         */
        public void Draw(SpriteBatch spriteBatch)
        {
            var scoreText = string.Format("{0}:{1}", this.PlayerScore, this.ComputerScore);
            var positionX = (this.gameBoundaries.Height * 0) + 200;
            var positionY = (this.gameBoundaries.Width * 0) + 15;
            var position = new Vector2(positionX, positionY);

            spriteBatch.DrawString(this.font, scoreText, position, Color.WhiteSmoke);
        }
    }
}
